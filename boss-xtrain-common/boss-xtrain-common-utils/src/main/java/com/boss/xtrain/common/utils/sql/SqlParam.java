package com.boss.xtrain.common.utils.sql;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 数据库参数
 * @author LGX_TvT
 * @date 2019-12-18 12:54
 */
@Data
@Component
@ConfigurationProperties(prefix = "bossoft.sql")
public class SqlParam {

    /**
     * 数据库驱动
     */
    private String sqlDriver;

    /**
     * URL路径
     */
    private String sqlUrl;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 密码
     */
    private String password;

}
