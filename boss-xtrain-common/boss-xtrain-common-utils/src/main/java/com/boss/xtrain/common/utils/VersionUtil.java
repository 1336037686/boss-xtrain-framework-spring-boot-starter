package com.boss.xtrain.common.utils;

import com.boss.xtrain.common.exception.logging.enums.ErrorCode;
import com.boss.xtrain.common.exception.logging.exception.BussinessException;
import com.boss.xtrain.core.data.convention.common.BaseDTO;

/**
 * 检查dto版本和数据库版本
 * @author LGX_TvT
 * @date 2019-12-18 10:37
 */
public class VersionUtil {

    /**
     * 版本检查
     * @param baseDTO
     * @param version
     * @return
     */
    public static boolean checkVersion(BaseDTO baseDTO, String version){
        if (baseDTO==null || version==null){
            throw new BussinessException(ErrorCode.SYSTEM_VERSION_ERROR, "版本为空");
        }
        if (baseDTO.getVersion()!=null && baseDTO.getVersion().equals(version)){
            return true;
        }else {
            throw new BussinessException(ErrorCode.SYSTEM_VERSION_ERROR, "版本不一致");
        }
    }
}
