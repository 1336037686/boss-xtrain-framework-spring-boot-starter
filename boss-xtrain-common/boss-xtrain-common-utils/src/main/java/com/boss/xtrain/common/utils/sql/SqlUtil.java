package com.boss.xtrain.common.utils.sql;

import com.boss.xtrain.common.exception.logging.enums.ErrorCode;
import com.boss.xtrain.common.exception.logging.exception.BussinessException;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.jdbc.ScriptRunner;

import java.io.IOException;
import java.io.Reader;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * SQL工具类
 * 主要用来执行
 * @author LGX_TvT
 * @date 2019-12-18 12:38
 */
public class SqlUtil {

    private SqlParam sqlParam;

    public SqlUtil(SqlParam sqlParam) {
        this.sqlParam = sqlParam;
    }


    /**
     * 使用sql创建数据库
     * @param sqlParam    sql参数
     * @param createDBSql sql语句
     * @return 是否创建成功
     */
    public boolean createDB(SqlParam sqlParam, String createDBSql) {
        Connection conn = createConnection(sqlParam);
        try {
            return execute(conn, createDBSql);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BussinessException(ErrorCode.SYSTEM_DB_CREATE_ERROR, "数据库创建失败");
        } finally {
            close(conn, null);
        }
    }

    /**
     * 建表
     * @param sqlParam
     * @param sql
     * @return
     */
    public boolean createTable(SqlParam sqlParam, String sql) {
        Connection conn = createConnection(sqlParam);
        try {
            return execute(conn, sql);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BussinessException(ErrorCode.SYSTEM_DB_TABLE_CREATE_ERROR, "创建表错误");
        } finally {
            close(conn, null);
        }
    }

    /**
     * 插入数据
     * @param sqlParam
     * @param sql
     * @return
     */
    public boolean insertDate(SqlParam sqlParam, String sql) {
        Connection conn = createConnection(sqlParam);
        try {
            return execute(conn, sql);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BussinessException(ErrorCode.SYSTEM_DB_SQL_ERROR, "增加数据错误");
        } finally {
            close(conn, null);
        }
    }

    /**
     * 执行自定义sql语句
     * @param sqlParam
     * @param sql
     * @return
     */
    public boolean executeSQL(SqlParam sqlParam, String sql) {
        Connection conn = createConnection(sqlParam);
        try {
            return execute(conn, sql);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BussinessException(ErrorCode.SYSTEM_DB_SQL_ERROR, "执行SQL出现错误");
        } finally {
            close(conn, null);
        }
    }

    /**
     * 执行sql脚本
     * @param sqlParam 数据库链接参数
     * @param reader sql文件输入流
     * @return
     */
    public boolean executeSQL(SqlParam sqlParam, Reader reader) {
        Connection conn = createConnection(sqlParam);
        try {
            ScriptRunner runner = new ScriptRunner(conn);
            //设置字符集,不然中文乱码插入错误
            Resources.setCharset(Charset.forName("UTF-8"));
            //设置是否输出日志
            runner.setLogWriter(null);
            runner.runScript(reader);
            runner.closeConnection();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new BussinessException(ErrorCode.SYSTEM_DB_SQL_ERROR, "执行SQL出现错误");
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            close(conn, null);
        }
    }

    /**
     * 执行SQL
     * @param connection
     * @param sql
     * @return
     * @throws SQLException
     */
    private boolean execute(Connection connection, String sql) throws SQLException {
        Statement stmt = connection.createStatement();
        if (stmt.executeUpdate(sql) == 0) {
            return true;
        }
        ScriptRunner runner = new ScriptRunner(connection);
        close(null, stmt);
        return false;
    }

    /**
     * 创建连接
     * @param sqlParam sql参数
     * @return
     */
    private Connection createConnection(SqlParam sqlParam) {
        try {
            Class.forName(sqlParam.getSqlDriver());
            Connection conn = DriverManager.getConnection(sqlParam.getSqlUrl(), sqlParam.getUserName(), sqlParam.getPassword());
            return conn;
        } catch (Exception e) {
            e.printStackTrace();
            throw new BussinessException(ErrorCode.SYSTEM_DB_CONNECTION_ERROR, "连接失败");
        }
    }

    /**
     * 关闭连接
     * @param connection
     * @param statement
     */
    private void close(Connection connection, Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
