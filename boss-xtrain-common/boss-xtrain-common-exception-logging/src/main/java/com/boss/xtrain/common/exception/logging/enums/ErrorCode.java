package com.boss.xtrain.common.exception.logging.enums;

/**
 * 错误状态码
 * @author LGX_TvT
 * @date 2019-11-29 10:56
 */
public final class ErrorCode {

    // ################# 子模块 任务 #################
    /**
     * 成功
     */
    public static final String SUCCEED = "0";

    /**
     * 系统版本错误
     */
    public static final String SYSTEM_VERSION_ERROR = "100008";

    /**
     * 系统缓存错误
     */
    public static final String SYSTEM_CACHE_ERROR = "100009";

    /**
     * 类型转换错误
     */
    public static final String SYSTEM_CONVERTER_ERROR = "100010";

    /**
     * 网关错误
     */
    public static final String SYSTEM_GATEWAY_ERROR = "100011";

    /**
     * 认证/授权错误
     */
    public static final String SYSTEM_AUTHORIZE_ERROR = "100012";

    /**
     * CDN错误
     */
    public static final String SYSTEM_CDN_ERROR = "130101";

    /**
     * SMS错误
     */
    public static final String SYSTEM_SMS_ERROR = "100014";

    /**
     * Email错误
     */
    public static final String SYSTEM_EMAIL_ERROR = "100015";

    /**
     * 基础数据服务错误
     */
    public static final String SYSTEM_BASIC_DATA_ERROR = "100016";

    /**
     * 数据库创建错误
     */
    public static final String SYSTEM_DB_CREATE_ERROR = "100017";

    /**
     * 数据库表创建错误
     */
    public static final String SYSTEM_DB_TABLE_CREATE_ERROR = "100018";

    /**
     * 数据库连接错误
     */
    public static final String SYSTEM_DB_CONNECTION_ERROR = "100019";

    /**
     * 数据库SQL语句执行错误
     */
    public static final String SYSTEM_DB_SQL_ERROR = "100020";

    /**
     * Exception其他异常
     */
    public static final String SYSTEM_EXCEPTION_ERROR = "100999";
}
