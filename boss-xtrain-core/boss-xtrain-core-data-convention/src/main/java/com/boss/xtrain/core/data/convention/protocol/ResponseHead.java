package com.boss.xtrain.core.data.convention.protocol;

import lombok.Data;

import java.io.Serializable;

/**
 * 响应消息头
 * @author LGX_TvT
 * @date 2019-11-28 20:41
 */
@Data
public class ResponseHead implements Serializable {

    /**
     * 版本
     */
    private String version;

    /**
     * 应答码
     */
    private String responseCode;

    /**
     * 消息
     */
    private String message;

    /**
     * 加密标志
     */
    private Integer encrypt = 0;

    public ResponseHead(){}

    public ResponseHead(String version, String responseCode, String message) {
        this.version = version;
        this.responseCode = responseCode;
        this.message = message;
    }

    public ResponseHead(String version, String responseCode, String message, Integer encrypt) {
        this.version = version;
        this.responseCode = responseCode;
        this.message = message;
        this.encrypt = encrypt;
    }
}
