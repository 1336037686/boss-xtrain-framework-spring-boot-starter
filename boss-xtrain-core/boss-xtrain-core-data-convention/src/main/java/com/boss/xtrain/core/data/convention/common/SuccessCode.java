package com.boss.xtrain.core.data.convention.common;

/**
 * 成功应答码
 * @author LGX_TvT
 * @date 2019-12-19 9:58
 */
public final class SuccessCode {

    /**
     * 成功状态码
     */
    public static final String SUCCESS_CODE = "999999";
}
