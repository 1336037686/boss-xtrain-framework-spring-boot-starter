package com.boss.xtrain.core.data.convention.common;

import lombok.Data;

import java.io.Serializable;

/**
 * 基础实体类
 * @ClassName: BaseEntity
 * @Description: ${description}
 * @Author: lang
 * @Date: 2019/12/5 15:46
 * @Version: v1.0
 */
@Data
public class BaseEntity implements Serializable {

    /**
     * 状态
     */
    private Byte status;

    /**
     * 创建人
     */
    private Long createdBy;

    /**
     * 创建时间
     */
    private String createdTime;

    /**
     * 更新人
     */
    private Long updatedBy;

    /**
     * 更新时间
     */
    private String updatedTime;

    /**
     * 版本
     */
    private Long version;

}
