package com.boss.xtrain.core.data.convention.protocol;

import lombok.Data;

import java.io.Serializable;

/**
 * 请求头，数据结构
 * @author LGX_TvT
 * @date 2019-11-28 20:41
 */
@Data
public class RequestHead implements Serializable {
    /**
     * 版本
     */
    private String version;

    /**
     * token
     */
    private String token;

    /**
     * 业务类型
     */
    private String businessType;


    /**
     * 设备ID
     */
    private String equipId;


    /**
     * 设备类型
     */
    private Integer equipType;


    /**
     * 加密标志
     */
    private Integer encrypt = 0;

    public RequestHead() {
    }

    public RequestHead(String version, String token, String businessType, String equipId, Integer equipType) {
        this.version = version;
        this.token = token;
        this.businessType = businessType;
        this.equipId = equipId;
        this.equipType = equipType;
    }

    public RequestHead(String version, String token, String businessType, String equipId, Integer equipType, Integer encrypt) {
        this.version = version;
        this.token = token;
        this.businessType = businessType;
        this.equipId = equipId;
        this.equipType = equipType;
        this.encrypt = encrypt;
    }
}
