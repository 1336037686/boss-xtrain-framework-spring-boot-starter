package com.boss.xtrain.core.data.convention.common;

import lombok.Data;

/**
 * 查询VO
 * @author LGX_TvT
 * @date 2019-12-18 11:36
 */
@Data
public class BaseQueryVO {
    /**
     * 当前请求页
     */
    private Integer currentPage;
    /**
     * 页面显示条数
     */
    private Integer pageSize;
    /**
     * 总页数
     */
    private Integer totalPages;

    public BaseQueryVO() {

    }

    public BaseQueryVO(int currentPage, int pageSize, int totalPages) {
        this.currentPage = currentPage;
        this.pageSize = pageSize;
        this.totalPages = totalPages;
    }
}
