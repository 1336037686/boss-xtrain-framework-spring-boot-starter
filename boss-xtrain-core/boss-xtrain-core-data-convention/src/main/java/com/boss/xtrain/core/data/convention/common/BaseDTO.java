package com.boss.xtrain.core.data.convention.common;

import lombok.Data;

import java.io.Serializable;

/**
 * 基础DTO文件
 * 公共字段
 * @ClassName: BaseDTO
 * @Description: ${description}
 * @Author: lang
 * @Date: 2019/12/5 15:45
 * @Version: v1.0
 */
@Data
public class BaseDTO implements Serializable {
    /**
     * 公司ID
     */
    private String companyId;

    /**
     * 机构ID
     */
    private String orgId;

    /**
     * Tocken
     */
    private String token;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private String createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private String updatedTime;

    /**
     * 版本
     */
    private String version;
}
