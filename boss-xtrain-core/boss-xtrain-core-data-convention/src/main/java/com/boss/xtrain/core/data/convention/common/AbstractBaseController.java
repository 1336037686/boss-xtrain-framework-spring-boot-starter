package com.boss.xtrain.core.data.convention.common;

import com.boss.xtrain.core.data.convention.protocol.CommonRequest;
import com.boss.xtrain.core.data.convention.protocol.CommonResponse;
import com.boss.xtrain.core.data.convention.protocol.ResponseBody;
import com.boss.xtrain.core.data.convention.protocol.ResponseHead;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;

import java.io.File;
import java.io.FileInputStream;

/**
 * Controller基类，定义CRUD以及一些基础操作
 * @ClassName: BaseController
 * @Description: ${description}
 * @Author: lang
 * @Date: 2019/12/4 9:44
 * @Version: v1.0
 */
public abstract class AbstractBaseController<T> {

    /**
     * 保存
     * @param commonRequest 前端统一请求包含保存的对象
     * @return 成功返回 <code>true</code> 否则 <code>false</code>
     */
    public abstract CommonResponse doSave(CommonRequest<T> commonRequest);

    /**
     * 更新数据
     * @param commonRequest 前端统一请求包含变更的对象
     * @return 成功返回 <code>true</code> 否则 <code>false</code>
     */
    public abstract CommonResponse doUpdate(CommonRequest<T> commonRequest);

    /**
     * 通过id删除数据
     * @param commonRequest  前端统一请求包含被删除的Id
     * @return 成功返回 <code>true</code> 否则 <code>false</code>
     */
    public abstract CommonResponse doDelete(CommonRequest<T> commonRequest);

    /**
     * 通过id查询数据
     * @param commonRequest 前端统一请求包含id
     * @return 查询出来的对象
     */
    public abstract CommonResponse doQuery(CommonRequest<T> commonRequest);

    /**
     * 封装应答包
     */
    protected CommonResponse buildCommponResponse(ResponseHead responseHead, Object object) {
        CommonResponse commonResponse = new CommonResponse();
        //调用工具类设置版本等信息
        commonResponse.setHead(responseHead);
        //封装body
        ResponseBody<Object> responseBody=new ResponseBody<>(object);
        commonResponse.setBody(responseBody);
        return commonResponse;
    }

    /**
     * 是否是windows系统
     *
     * @return
     */
    private static boolean isWindows() {
        String osName = System.getProperty("os.name");
        if (osName != null && osName.toLowerCase().indexOf("win") >= 0) {
            return true;
        }
        return false;
    }

    /**
     *  查看pom.xml版本号
     * @return
     */
    public String getVersion(String pom) {
        MavenXpp3Reader reader = new MavenXpp3Reader();
        try{
            FileInputStream fis = new FileInputStream(new File(pom));
            Model model = reader.read(fis);
            return model.getVersion();
        }
        catch (Exception e){
            return null;
        }
    }


}

