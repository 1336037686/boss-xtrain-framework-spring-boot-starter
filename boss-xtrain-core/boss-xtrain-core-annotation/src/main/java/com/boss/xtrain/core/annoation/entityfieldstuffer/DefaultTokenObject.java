package com.boss.xtrain.core.annoation.entityfieldstuffer;

import org.springframework.stereotype.Component;

/**
 * @author wulei
 */
@Component
public class DefaultTokenObject implements TokenObject {
    @Override
    public  String getId(){
        return "tenantId";
    }
}
