package com.boss.xtrain.core.annoation.entityfieldstuffer;

import java.lang.annotation.*;

/**新增操作字段注解
 * @author wulei
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EntityStufferBeforeController {
    String value() default "save";
}
