package com.boss.xtrain.core.annoation.entityfieldstuffer;

import java.lang.annotation.*;

/**
 * 2 * @Author: wulei
 * 3 * @Date: 2019/12/13 15:03
 * 4
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EntityStufferBeforeService {
    String value() default "save";
}
