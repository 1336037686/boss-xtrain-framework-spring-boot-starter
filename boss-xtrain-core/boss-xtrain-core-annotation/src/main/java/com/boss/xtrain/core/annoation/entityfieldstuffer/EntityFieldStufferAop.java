package com.boss.xtrain.core.annoation.entityfieldstuffer;


import com.boss.xtrain.common.utils.TokenUtil;
import com.boss.xtrain.core.data.convention.common.BaseDTO;
import com.boss.xtrain.core.data.convention.protocol.CommonRequest;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.hibernate.validator.internal.util.privilegedactions.NewInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**公共字段注入切面
 * @author wulei
 */
@Slf4j
@Aspect
@Component
public class EntityFieldStufferAop {

    @Autowired
    private TokenObject tokenObject;

    @Pointcut("@annotation(com.boss.xtrain.core.annoation.entityfieldstuffer.EntityStufferBeforeController)")
    public void entityStufferBeforeController(){}

    @Pointcut("@annotation(com.boss.xtrain.core.annoation.entityfieldstuffer.EntityStufferBeforeService)")
    public void entityStufferBeforeService(){}

    private  final String SAVE_ENTITY="save";

    private  final String UPDATE_ENTITY="update";

    @Around("entityStufferBeforeController()")
    public Object entityStufferBeforeController(ProceedingJoinPoint pjp) throws Throwable {
        Object result=null;
        Object[] obj  = pjp.getArgs();
        Signature signature = pjp.getSignature();

        MethodSignature methodSignature = (MethodSignature) signature;
        EntityStufferBeforeController entityStuffer = methodSignature.getMethod().getAnnotation(EntityStufferBeforeController.class);
        CommonRequest commonRequest=(CommonRequest)obj[0];
        Object vo=commonRequest.getBody().getData().getClass().newInstance();
        vo=commonRequest.getBody().getData();

        Field[] fields1 = vo.getClass().getSuperclass().getDeclaredFields();

        String token= commonRequest.getHead().getToken();
        Map<String,Object> tokenMap=TokenUtil.verifyToken(token);
        SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString=formatter.format(new Date());
        if(SAVE_ENTITY.equals(entityStuffer.value())){
            tokenMap.put("createdBy",tokenMap.get(tokenObject.getId()));
            tokenMap.put("createdTime",dateString);
            tokenMap.put("updatedBy",tokenMap.get(tokenObject.getId()));
            tokenMap.put("updatedTime",dateString);
        }
        else {
            tokenMap.put("updatedTime",dateString);
            tokenMap.put("updatedBy",tokenMap.get(tokenObject.getId()));
        }
        for (Field field : fields1) {
            field.setAccessible(true);
            if(tokenMap.containsKey(field.getName())){

                field.set(vo, tokenMap.get(field.getName()));
            }
        }
          /*  maps.putAll(tokenMap);
            String value=entityStuffer.value();

            if(SAVE_ENTITY.equals(value)){
                Field[] fields = obj.getClass().getSuperclass().getDeclaredFields();
                for (Field field : fields) {
                    field.setAccessible(true);
                    if(tokenMap.containsKey(field.getName())){

                        field.set(obj, tokenMap.get(field.getName()));
                    }
                }

                maps.put("createdBy",tokenMap.get(tokenObject.getId()));
                maps.put("createdTime",dateString);
                maps.put("updatedBy",tokenMap.get(tokenObject.getId()));
                maps.put("updatedTime",dateString);
            }
            else {
                maps.put("updatedTime",dateString);
                maps.put("updatedBy",tokenMap.get(tokenObject.getId()));
            }
      */
        commonRequest.getBody().setData(vo);
        obj[0]=commonRequest;
        Object msg=pjp.proceed(obj);
        return msg;
    }

    @Around("entityStufferBeforeService()")
    public Object saveEntityStuffer(ProceedingJoinPoint pjp) throws Throwable {
        Object result=null;
        if(pjp.getArgs().length==0){
            Object msg= pjp.proceed();
            return msg;
        }
        Signature signature = pjp.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        EntityStufferBeforeService entityStuffer = methodSignature.getMethod().getAnnotation(EntityStufferBeforeService.class);

        Object obj  = pjp.getArgs()[0];
        BaseDTO baseDto=(BaseDTO) obj;
        String token=baseDto.getToken();

        Map<String,Object> tokenMap=TokenUtil.verifyToken(token);
        SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString=formatter.format(new Date());
        if(SAVE_ENTITY.equals(entityStuffer.value())){
            tokenMap.put("createdBy",tokenMap.get(tokenObject.getId()));
            tokenMap.put("createdTime",dateString);
            tokenMap.put("updatedBy",tokenMap.get(tokenObject.getId()));
            tokenMap.put("updatedTime",dateString);
        }
        else {
            tokenMap.put("updatedTime",dateString);
            tokenMap.put("updatedBy",tokenMap.get(tokenObject.getId()));
        }
        Field[] fields = obj.getClass().getSuperclass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            if(tokenMap.containsKey(field.getName())){

                field.set(obj, tokenMap.get(field.getName()));
            }
        }
        Object msg=pjp.proceed(pjp.getArgs());
        return msg;
    }
}

