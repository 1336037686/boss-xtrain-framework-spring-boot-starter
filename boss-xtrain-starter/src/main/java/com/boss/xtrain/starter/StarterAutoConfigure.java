package com.boss.xtrain.starter;

import com.aliyun.oss.OSSClient;
import com.boss.xtrain.common.utils.cdn.CdnParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Starter
 * @author LGX_TvT
 * @date 2019-12-18 13:58
 */
@ComponentScan(basePackages = {"com.boss.xtrain"})
@Configuration
public class StarterAutoConfigure {

    @Autowired
    CdnParam cdnParam;
    /**
     * OSSClient ossClient = new OSSClient(endPoint,accessKeyId, accessKeySecret)
     * @return
     */
    @Bean
    @ConditionalOnMissingBean(OSSClient.class)
    public OSSClient ossClient(){
        return new OSSClient(cdnParam.getEndPoint(), cdnParam.getAccessKeyId(), cdnParam.getAccessKeySelect());
    }
}
