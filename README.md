# boss-xtrain-framework-spring-boot-starter

# 1、基础框架设计



## Swagger2/Cdn/Sms/Email基础配置
> 具体属性介绍参照如下类
 - Swagger2 : com.boss.xtrain.core.data.convention.apidoc.Swagger2Param
 - Cdn : com.boss.xtrain.common.utils.cdn.CdnParam
 - Sms : com.boss.xtrain.common.utils.sms.SmsParam
 - Email: com.boss.xtrain.common.utils.email.EmailParam

具体配置：

```properties
# Swagger2配置
bossoft.swagger2.basePackage = com.lgx
bossoft.swagger2.title = "springboot利用swagger构建api文档"
bossoft.swagger2.description = "简单优雅的restful风格"
bossoft.swagger2.termsOfServiceUrl = "https://blog.csdn.net/l1336037686"
bossoft.swagger2.version = v1.0

# Cdn配置
bossoft.cdn.endPoint = oss-cn-shanghai.aliyuncs.com
bossoft.cdn.accessKeyId = LTAIGTcfXJNBfkTM
bossoft.cdn.accessKeySelect = jS1vjMOQfV35Jbo7ULDNRiyz5qZ2mM"
bossoft.cdn.bucketName = pkying
bossoft.cdn.folder = image/

# Sms短信服务配置
bossoft.sms.defaultConnectTimeout = sun.net.client.defaultConnectTimeout
bossoft.sms.defaultReadTimeout = sun.net.client.defaultReadTimeout
bossoft.sms.timeout = 20000
bossoft.sms.product = Dysmsapi
bossoft.sms.domain = dysmsapi.aliyuncs.com
bossoft.sms.accessKeyId = LTAIGTcfXJNBfkTM
bossoft.sms.accessKeySecret = jS1vjMOQfV35Jbo7ULDNRiyz5qZ2mM
bossoft.sms.signName = SaltFish
bossoft.sms.templateCode = SMS_172738481
bossoft.sms.regionId = cn-hangzhou
bossoft.sms.smsCodeLength = 6

# Email邮件服务
bossoft.email.endPoint = oss-cn-shanghai.aliyuncs.com
bossoft.email.accessKeyId = LTAIGTcfXJNBfkTM
bossoft.email.accessKeySelect = jS1vjMOQfV35Jbo7ULDNRiyz5qZ2mM
bossoft.email.accountName = postmaste@pkying.top
bossoft.email.tagName = postmaster
bossoft.email.fromAlias = 线上考试系统
bossoft.email.toAddress = true
bossoft.email.subject = 线上考试系统
bossoft.email.emailHtmlBody = <h1 style="color:red;">{{code}}</h1>
bossoft.email.regionId = cn-hangzhou
bossoft.email.emailCodeLength = 4
```
yml文件
```yml
spring:
  application:
    name: upfs-provider
  datasource:
    name: dataSource1                                                             #如果存在多个数据源，监控的时候可以通过名字来区分开来。如果没有配置，将会生成一个名字，格式是："DataSource-" + System.identityHashCode(this)
    type: com.alibaba.druid.pool.DruidDataSource
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://106.15.205.51:3306/lease_db?serverTimezone=UTC&characterEncoding=utf-8
    username: root
    password: root
    # druid配置
    druid:
      initialSize: 10                                                              #初始化连接个数
      minIdle: 10                                                                  #最小空闲连接个数
      maxActive: 100                                                               #最大连接个数
      maxWait: 60000                                                               #获取连接时最大等待时间，单位毫秒。
      timeBetweenEvictionRunsMillis: 60000                                         #配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
      minEvictableIdleTimeMillis: 30000                                            #配置一个连接在池中最小生存的时间，单位是毫秒
      validationQuery: select 'x'                                                  #用来检测连接是否有效的sql，要求是一个查询语句。
      testWhileIdle: true                                                          #建议配置为true，不影响性能，并且保证安全性。如果空闲时间大于timeBetweenEvictionRunsMillis，执行validationQuery检测连接是否有效。
      testOnBorrow: true                                                           #申请连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能
      testOnReturn: false                                                          #归还连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能
      poolPreparedStatements: false                                                #是否缓存preparedStatement，也就是PSCache。PSCache对支持游标的数据库性能提升巨大，比如说oracle。在mysql下建议关闭。
      maxPoolPreparedStatementPerConnectionSize: -1                                #要启用PSCache，必须配置大于0，当大于0时，poolPreparedStatements自动触发修改为true。在Druid中，不会存在Oracle下PSCache占用内存过多的问题，可以把这个数值配置大一些，比如说100
      filters: stat,wall                                                           #通过别名的方式配置扩展插件，常用的插件有：监控统计用的filter:stat，日志用的filter:log4j，防御sql注入的filter:wall
      connectionProperties: druid.stat.mergeSql=true;druid.stat.slowSqlMillis=5000 # 通过connectProperties属性来打开mergeSql功能；慢SQL记录
      useGlobalDataSourceStat: false                                               # 合并多个DruidDataSource的监控数据
  # redis 配置
  redis:
    database: 0                                                                    # Redis数据库索引（默认为0）
    host: 106.15.205.51
    port: 6379
    password:
    redis:
      pool:
        max-active: 200                                                            # 最大连接数
        max-wait: -1                                                               # 最大阻塞等待时间
        max-idle: 10                                                               # 最大空闲连接
        min-idle: 0                                                                # 最小空闲连接
    timeout: 1000                                                                  # 连接超时时间

# Mybatis配置
mybatis:
  config-location: classpath:mybatis/mybatis-config.xml # 指定全局配置文件的位置
  mapper-locations: classpath:mybatis/mapper/*.xml      # 指定sql映射文件的位置

# 服务器配置
server:
  port: 8080

# 日志
logging:
  pattern:
    console: "%d{yyyy/MM/dd-HH:mm:ss} [%thread] %-5level %logger- %msg%n"
  file:
    name: applicationRun.log

# 基础框架配置
bossoft:
  # Swagger2配置
  swagger2:
    basePackage: com.boss.bes
    title: 租赁平台api文档
    description: 简单优雅的restful风格
    termsOfServiceUrl: https://blog.csdn.net/l1336037686
    version: v1.0
  # Cdn配置
  cdn:
    endPoint: oss-cn-shanghai.aliyuncs.com
    accessKeyId: LTAIGTcfXJNBfkTM
    accessKeySelect: jS1vjMOQfV35Jbo7ULDNRiyz5qZ2mM
    bucketName: pkying
    folder: image/
  # Sms短信服务配置
  sms:
    defaultConnectTimeout: sun.net.client.defaultConnectTimeout
    defaultReadTimeout: sun.net.client.defaultReadTimeout
    timeout: 20000
    product: Dysmsapi
    domain: dysmsapi.aliyuncs.com
    accessKeyId: LTAIGTcfXJNBfkTM
    accessKeySecret: jS1vjMOQfV35Jbo7ULDNRiyz5qZ2mM
    signName: SaltFish
    templateCode: SMS_172738481
    regionId: cn-hangzhou
    smsCodeLength: 6
  # Email邮件服务
  email:
    endPoint: oss-cn-shanghai.aliyuncs.com
    accessKeyId: LTAIGTcfXJNBfkTM
    accessKeySelect: jS1vjMOQfV35Jbo7ULDNRiyz5qZ2mM
    accountName: postmaste@pkying.top
    tagName: postmaster
    fromAlias: 租赁系统
    toAddress: true
    subject: 租赁系统
    emailHtmlBody: <h1 style="color:red;">验证码：{{code}}</h1>
    regionId: cn-hangzhou
    emailCodeLength: 6
```

除了配置上述之外由于CDN服务需要，还需要自定义一个配置类
里面注入一个OSSClient对象,后期可以考虑优化，将其做成starter
```java
/**
 * @author LGX_TvT
 * @date 2019-12-18 0:09
 */
@Configuration
public class Config {

    @Autowired
    CdnParam cdnParam;
    /**
     * OSSClient ossClient = new OSSClient(endPoint,accessKeyId, accessKeySecret)
     * @return
     */
    @Bean
    public OSSClient ossClient(){
        return new OSSClient(cdnParam.getEndPoint(), cdnParam.getAccessKeyId(), cdnParam.getAccessKeySelect());
    }
}
```
